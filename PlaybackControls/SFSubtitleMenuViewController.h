//
//  SFSubtitleMenuViewController.h
//  MT2P
//
//  Created by Jack on 3/21/13.
//  Copyright (c) 2013 Jack. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SFSubtitleMenuDelegate <NSObject>
- (void) didSelectSubtitleAtIndex: (NSInteger) index;
@end

@class SFMovieSubtitle;
@interface SFSubtitleMenuViewController : UITableViewController
- (id) initWithSubtitle: (SFMovieSubtitle*) subtitle;
@property (nonatomic, weak) id<SFSubtitleMenuDelegate> delegate;
@end
